#ifndef AGV_H_
#define AGV_H_

#include "DebugConsleUsart.h"
#include "delay.h"

#define AGV_USART USART2
#define AGV_USART_RCC RCC_APB1Periph_USART2
#define AGV_USART_GPIO GPIOA
#define AGV_USART_GPIO_RCC RCC_APB2Periph_GPIOA
#define AGV_USART_TX_PIN GPIO_Pin_2
#define AGV_USART_RX_PIN GPIO_Pin_3

typedef enum
{
    AGV_Seccess = 0,
    AGV_Fail = 1
} AGV_State;

//CRC校验
uint32_t crc16(uint8_t *puchMsg, uint8_t usDataLen);

/******************************
 * Func         AGV_ReadData()
 * brief        读AGV寄存器
 * param        AGV_Addr    AGV设备地址
 * param        AGV_Geg     AGV寄存器起始地址
 * param        dataNum     读寄存器数量
 * param        data        数据指针
 * return       AGV_State	状态返回值
 ******************************/
AGV_State AGV_Init(void);

/******************************
 * Func         AGV_ReadData()
 * brief        读AGV寄存器
 * param        AGV_Addr    AGV设备地址
 * param        AGV_Geg     AGV寄存器起始地址
 * param        dataNum     读寄存器数量
 * param        data        数据指针
 * return       AGV_State	状态返回值
 ******************************/
AGV_State AGV_ReadData(uint8_t AGV_Addr, uint16_t AGV_Reg, uint16_t dataNum, uint16_t *data);

/******************************
 * Func         AGV_WriteData()
 * brief        写单个AGV寄存器
 * param        AGV_Addr    AGV设备地址
 * param        AGV_Geg     AGV寄存器起始地址
 * param        data     	写寄存器数据
 * return       AGV_State	状态返回值
 ******************************/
AGV_State AGV_WriteData(uint8_t AGV_Addr, uint16_t AGV_Reg, uint16_t data);

/******************************
 * Func         AGV_GetStates()
 * brief        获取传感器检测值
 * param        AGV_Addr    AGV设备地址
 * param        data        数据指针
 * return       AGV_State	状态返回值
 ******************************/
AGV_State AGV_GetStates(uint8_t AGV_Addr, uint8_t *data);

#endif
