#ifndef _SERVO_H_
#define _SERVO_H_

#include "stdint.h"

void Servo_Init(void);

void Servo_DirSet(uint16_t direction);

void Servo_MidDirSet(int16_t direction);

#endif
