#ifndef __LED_H
#define __LED_H

#define LED_ON GPIO_ResetBits(GPIOC, GPIO_Pin_13)
#define LED_OFF GPIO_SetBits(GPIOC, GPIO_Pin_13)

void led_init(void);

#endif
