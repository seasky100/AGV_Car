#ifndef __KEY_H
#define __KEY_H

#include "stdint.h"

#define RTE_DEVICE_STDPERIPH_EXTI

#define KEY0 GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_10)
#define KEY1 GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11)
#define KEY2 GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12)
#define KEY3 GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_13)

extern uint8_t running_mode;
extern int motor, dir;
uint8_t get_key(void);
void key_init(void);
void EXTIX_Init(void);
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI3_IRQHandler(void);

#endif
