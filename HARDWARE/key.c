#define RTE_DEVICE_STDPERIPH_EXTI
#include "stm32f10x.h"
#include "key.h"
#include "delay.h"

int motor, dir;
uint8_t running_mode = 0;

void key_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

uint8_t get_key()
{
	if (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8))
		return 1;
	if (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_9))
		return 2;
	if (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_10))
		return 3;
	if (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_11))
		return 4;
	if (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_12))
		return 5;
	return 0;
}

void EXTIX_Init(void)
{

	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	key_init(); //	按键端口初始化

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); //使能复用功能时钟

	//GPIOB.10 		中断线以及中断初始化配置	下降沿触发	KEY0
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource10);
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	//GPIOB.11	  	中断线以及中断初始化配置	下降沿触发	KEY1
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource11);
	EXTI_InitStructure.EXTI_Line = EXTI_Line1;
	EXTI_Init(&EXTI_InitStructure);

	//GPIOB.12	  	中断线以及中断初始化配置	下降沿触发	KEY2
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource12);
	EXTI_InitStructure.EXTI_Line = EXTI_Line2;
	EXTI_Init(&EXTI_InitStructure);

	//GPIOB.13	  	中断线以及中断初始化配置	下降沿触发	KEY3
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource13);
	EXTI_InitStructure.EXTI_Line = EXTI_Line3;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;			 //使能按键KEY0所在的外部中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; //抢占优先级2，
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;		 //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				 //使能外部中断通道
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;			 //使能按键KEY1所在的外部中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; //抢占优先级2，
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;		 //子优先级2
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				 //使能外部中断通道
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;			 //使能按键KEY2所在的外部中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; //抢占优先级2
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;		 //子优先级1
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				 //使能外部中断通道
	NVIC_Init(&NVIC_InitStructure);								 //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器

	NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;			 //使能按键KEY3所在的外部中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02; //抢占优先级2
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;		 //子优先级0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				 //使能外部中断通道
	NVIC_Init(&NVIC_InitStructure);								 //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器
}

//外部中断0服务程序
void EXTI0_IRQHandler(void)
{
	delay_ms(10);
	if (KEY0 == 0)
	{
		running_mode = 0;
	}
	EXTI_ClearITPendingBit(EXTI_Line0); //清除LINE0上的中断标志位
}

//外部中断1服务程序
void EXTI1_IRQHandler(void)
{
	delay_ms(10);
	if (KEY1 == 0)
	{
		running_mode = 1;
	}
	EXTI_ClearITPendingBit(EXTI_Line1); //清除LINE1上的中断标志位
}

//外部中断2服务程序
void EXTI2_IRQHandler(void)
{
	delay_ms(10);
	if (KEY2 == 0)
	{
		running_mode = 2;
	}
	EXTI_ClearITPendingBit(EXTI_Line2); //清除LINE2上的中断标志位
}

//外部中断3服务程序
void EXTI3_IRQHandler(void)
{
	delay_ms(10);
	if (KEY3 == 0)
	{
		running_mode = 3;
	}
	EXTI_ClearITPendingBit(EXTI_Line3); //清除LINE3上的中断标志位
}
