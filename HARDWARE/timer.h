#ifndef __TIMER_H
#define __TIMER_H

#include "stdint.h"

void TIM3_Int_Init(uint16_t arr, uint16_t psc);
void TIM4_Int_Init(uint16_t arr, uint16_t psc);
void TIM1_PWM_Init(uint16_t arr, uint16_t psc);
void TIM3_PWM_Init(uint16_t arr, uint16_t psc);
#endif
