#ifndef __MOTOR_H
#define __MOTOR_H

void RCC_Configuration(void);
void GPIO_Configuration(void);
void TIM_Configuration(void);
void TIM_OC_Configration(void);
void motor_init(void);

#endif
