#ifndef __USART_H
#define __USART_H
#include "stdio.h"
#include "stdint.h"
#include "stm32f10x_usart.h"
#define USART_REC_LEN 200
#define EN_USART3_RX 1

extern uint8_t DebugConsleUsart_RX_BUF[USART_REC_LEN];
extern uint16_t DebugConsleUsart_RX_STA;
void DebugConsleUsart_Init(uint32_t bound);
#endif
