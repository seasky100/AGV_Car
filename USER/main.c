#include "delay.h"
#include "led.h"
#include "key.h"
#include "DebugConsleUsart.h"
#include <stdio.h>
#include "servo.h"
#include "AGV.h"
#include "motor.h"
#include "oled.h"
#include "control.h"
#include "timer.h"

/*
	舵机
	电机
	OLED
	KEY
	LED
	AGV
*/

int main(void)
{
	int i = 0;
	delay_init(); //延时初始化
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	DebugConsleUsart_Init(115200); //终端调试串口初始化

	led_init();
	OLED_Init();
	motor_init();
	Servo_Init();
	Servo_MidDirSet(20);
	AGV_Init();
	runInit();
	TIM3_Int_Init(719, 499);

	for (;; i++)
	{
		stateDisplay();
	}
}
