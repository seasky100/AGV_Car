#include "remote.h"
#include "key.h"
#include "stm32f10x.h"

int rdir = 0, rmotor = 0, rmotorb = 0;
const uint8_t startstr[4] = {0xA5, 0x5A, 0x00};
void remote_mod(uint8_t *command)
{
	uint8_t i;
	uint8_t cdir, cspeed;
	for (i = 0; i < 2; i++)
	{
		if (startstr[i] != command[i])
			break;
	}
	if (i == 2)
	{
		cspeed = command[2];
		cdir = command[3];

		if (cspeed > 127)
		{
			rmotor = (cspeed - 128);
			rmotorb = 0;
		}
		else if (cspeed < 128)
		{
			rmotor = 0;
			rmotorb = (128 - cspeed);
		}

		rdir = (cdir - 128) / 5;
		dir = rdir;
		motor = cspeed - 128;
		;
		TIM_SetCompare1(TIM1, rmotor);  //电机1+
		TIM_SetCompare2(TIM1, rmotorb); //电机1-
		TIM_SetCompare3(TIM1, rmotor);  //电机2+
		TIM_SetCompare4(TIM1, rmotorb); //电机2-

		TIM_SetCompare1(TIM3, 135 + rdir); //舵机
	}
}
