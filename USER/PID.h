#ifndef _PID_h
#define _PID_h

#include <stdint.h>

#define PID_P ((uint8_t)0x01)
#define PID_I ((uint8_t)0x02)
#define PID_D ((uint8_t)0x04)

typedef struct structPIDControler
{
    double Kp;
    double Ki;
    double Kd;

    double target;
    double actrul;
    double lastError;
    double prevError;

    double sumError;
} PIDControl; //PID控制器参数结构体

void PID_Init(PIDControl *pid); //PID参数初始化

double PID_LocCal(PIDControl *pid, uint8_t Ctrl); //位置式PID
double PID_IncCal(PIDControl *pid, uint8_t Ctrl); //增量式PID

#endif
